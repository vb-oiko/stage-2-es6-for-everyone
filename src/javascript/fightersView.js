import View from "./view";
import FighterView from "./fighterView";
import DetailsView from "./detailsView";
import { fighterService } from "./services/fightersService";
import ArenaView from "./arenaView";

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
    this.createArena();
    // this.hideUncheckedFighters = this.hideUncheckedFighters.bind(this);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    this.fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });
    this.element.append(...this.fighterElements);
  }

  createArena() {
    const arenaView = new ArenaView(this);
    this.element.append(arenaView.element);
  }

  showAllFighters() {
    this.fighterElements.map(el => (el.style.display = "flex"));
  }

  hideUncheckedFighters() {
    const checked = Array.from(this.fightersDetailsMap.values())
      .filter(el => el.checked)
      .map(el => Number(el._id));

    this.fighterElements.map((el, ind) => {
      if (checked.indexOf(ind + 1) === -1) {
        el.style.display = "none";
      }
    });
  }

  changeHealthStatus() {}

  async handleFighterClick(event, fighter) {
    // get from map or load info and add to fightersMap
    if (!this.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(
        fighter._id
      );
      fighterDetails.checked = false;
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }

    // show modal with fighter info
    // allow to edit health and power in this modal
    if (event.target.tagName !== "INPUT") {
      const detailsView = new DetailsView(fighter._id, this.fightersDetailsMap);
      this.element.append(detailsView.element);
    } else {
      this.fightersDetailsMap.get(fighter._id).checked = event.target.checked;
    }
  }
}

export default FightersView;
