import View from "./view";

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkedElement = this.createCheckbox(name);
    // const healthElement = this.createHealthIndicator();

    this.element = this.createElement({
      tagName: "div",
      className: "fighter",
      attributes: { data_id: fighter._id }
    });
    this.element.append(
      imageElement,
      nameElement,
      checkedElement
      // healthElement
    );
    this.element.addEventListener(
      "click",
      event => handleClick(event, fighter),
      false
    );
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: "span",
      className: "name"
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: "img",
      className: "fighter-image",
      attributes
    });

    return imgElement;
  }

  createCheckbox(name) {
    const checkboxElement = this.createElement({
      tagName: "input",
      attributes: {
        type: "checkbox"
        // id: "check" + name
      }
    });

    return checkboxElement;
  }

  createHealthIndicator() {
    const healthElement = this.createElement({ tagName: "p" });
    healthElement.innerText = "Health = 100%";
    return healthElement;
  }
}

export default FighterView;
