import View from "./view";
import Fighter from "./fighter";

const MSG = {
  selectFighters: "Please select two fighters to start the game.",
  selectTwoFighters: "Please select TWO fighters to start the game.",
  startGame: "Game starting...",
  stopGame: "Game canceled.",
  fightBtn: {
    start: "Start Fight!",
    stop: "Stop Fight"
  }
};

class ArenaView extends View {
  constructor(fightersView) {
    super();
    this.fightersDetailsMap = fightersView.fightersDetailsMap;
    this.hideUncheckedFighters = fightersView.hideUncheckedFighters.bind(
      fightersView
    );
    this.showAllFighters = fightersView.showAllFighters.bind(fightersView);
    this.element = this.createElement({
      tagName: "div",
      attributes: { style: "flex-basis: 100%;" }
    });
    this.createArena();
    this.gameStatusElement.innerText = MSG.selectFighters;
    this.fightMode = false;
  }

  createFightButton() {
    this.fightBtn = this.createElement({
      tagName: "button",
      attributes: { style: "margin: 15px auto; display: block;" }
    });
    this.fightBtn.addEventListener("click", this.handleFightBtnClick);
    this.fightBtn.innerText = MSG.fightBtn.start;
    return this.fightBtn;
  }

  fight(attackFighter, defenseFighter) {
    const damage = Math.round(
      attackFighter.getHitPower() - defenseFighter.getBlockPower()
    );
    return damage < 0 ? 0 : damage;
  }

  changeGameStatus(status) {
    if (status) {
      console.log(status);
      this.gameStatusElement.innerText = status;
    }
  }

  createArena() {
    const fightBtnElement = this.createFightButton();
    this.element.append(fightBtnElement);

    this.gameStatusElement = this.createElement({
      tagName: "div",
      attributes: { style: "width: 100%; text-align: center;" }
    });

    this.changeGameStatus(MSG.gameStarting);
    this.element.append(this.gameStatusElement);
  }

  countSelectedFighters() {
    const selected = Array.from(this.fightersDetailsMap.values());
    return selected.reduce((acc, el) => (el.checked ? acc + 1 : acc), 0);
  }

  startGame() {
    this.changeGameStatus(MSG.startGame);
    this.fightMode = true;
    this.fightBtn.innerText = MSG.fightBtn.stop;

    this.fighters = Array.from(this.fightersDetailsMap.values())
      .filter(el => el.checked)
      .map(el => new Fighter(el));

    if (Math.random() - 0.5 > 0) {
      this.swapFighters();
    }

    this.changeGameStatus(`${this.fighters[0].name} starts the fight`);
    this.hideUncheckedFighters();

    this.intervalId = setInterval(this.nextTurn, 1000);
  }

  stopGame() {
    this.fightMode = false;
    clearInterval(this.intervalId);
    this.fightBtn.innerText = MSG.fightBtn.start;
    this.showAllFighters();
  }

  handleFightBtnClick = () => {
    if (this.fightMode) {
      this.changeGameStatus(MSG.stopGame);
      this.stopGame();
      this.fightBtn.innerText = MSG.fightBtn.start;
    } else {
      if (this.countSelectedFighters() === 2) {
        this.startGame();
      } else {
        this.changeGameStatus(MSG.selectTwoFighters);
      }
    }
  };

  swapFighters() {
    this.fighters = [this.fighters[1], this.fighters[0]];
  }

  nextTurn = () => {
    this.swapFighters();
    this.changeGameStatus("----------------------------");
    this.changeGameStatus(this.fighters[0]);
    this.changeGameStatus(this.fighters[1]);

    const damage = this.fight(this.fighters[0], this.fighters[1]);
    this.fighters[1].health -= damage;
    this.changeGameStatus(
      `${this.fighters[0].name} hits ${
        this.fighters[1].name
      } and causes ${damage} damage`
    );
    if (this.fighters[1].health <= 0) {
      this.changeGameStatus(`KO! ${this.fighters[0].name} wins!`);
      this.stopGame();
    }
  };
}

export default ArenaView;
