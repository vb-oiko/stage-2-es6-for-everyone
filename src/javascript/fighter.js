class Fighter {
  constructor(fighter) {
    this.name = fighter.name;
    this.health = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    console.log(this);
  }

  getHitPower() {
    const criticalHitChance = Math.random() + 1;
    const power = this.attack * criticalHitChance;
    console.log("Hit power: ", power);

    return power;
  }

  getBlockPower() {
    const dodgeChance = Math.random() + 1;
    const power = this.defense * dodgeChance;
    console.log("Block power: ", power);
    return power;
  }
}

export default Fighter;
