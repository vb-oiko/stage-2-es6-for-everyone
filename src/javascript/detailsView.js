import View from "./view";

class DetailsView extends View {
  constructor(fighterId, fightersDetailsMap) {
    super();

    Object.assign(this, { fighterId, fightersDetailsMap });

    this.createModal();
  }

  createModal() {
    this.element = this.createElement({
      tagName: "div",
      className: "details-overlay"
    });

    this.element.addEventListener("click", event => this.closeModal(event));

    const wrapElement = this.createElement({
      tagName: "div",
      className: "details-modal"
    });

    const nameElement = this.createElement({ tagName: "h3" });
    nameElement.innerText = this.fightersDetailsMap.get(this.fighterId).name;

    const inputElements = ["health", "attack", "defense"].map(prop =>
      this.createInput(
        prop,
        this.fightersDetailsMap.get(this.fighterId)[prop],
        "number"
      )
    );

    const btnOk = this.createButton("Ok", this.handleOkClick);
    const btnCancel = this.createButton("Cancel", this.handleCancelClick);

    wrapElement.append(nameElement, ...inputElements, btnOk, btnCancel);
    this.element.append(wrapElement);
  }

  createInput(propertyName, value, type) {
    const inputElement = this.createElement({
      tagName: "input",
      attributes: {
        id: propertyName,
        type,
        value
      }
    });

    const labelElement = this.createElement({
      tagName: "label",
      attributes: {
        for: propertyName
      }
    });
    labelElement.innerText = propertyName + ": ";

    const propertyElement = this.createElement({ tagName: "div" });
    propertyElement.append(labelElement, inputElement);

    return propertyElement;
  }

  createButton(name, handler) {
    const btnElement = this.createElement({
      tagName: "button",
      classname: name + "Btn"
    });

    btnElement.innerText = name;
    btnElement.addEventListener("click", handler);

    return btnElement;
  }

  closeModal(event) {
    if (!event || event.target === this.element) {
      this.element.parentNode.removeChild(this.element);
    }
  }

  handleOkClick = event => {
    const fighter = this.fightersDetailsMap.get(this.fighterId);

    fighter.health = document.getElementById("health").value;
    fighter.attack = document.getElementById("attack").value;
    fighter.defense = document.getElementById("defense").value;

    this.closeModal();
  };

  handleCancelClick = event => {
    this.closeModal();
  };
}

export default DetailsView;
